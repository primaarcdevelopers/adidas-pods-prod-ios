////////////////////////////////////////////////////
// CONFIGURATION
////////////////////////////////////////////////////

$(function() {
    $('body').removeClass('fade-out');
});

var globalCfg = {
  screensaverDelay : 80, // seconds original 80 seconds
  screensaverWarning : 70, // seconds
  screensaverInterval : 5000, // milliseconds
  checkConnectionDelay : 1000, // miliseconds
  backgroundImageCount : 2, // amount of bg images
  adminPassword : 'aditabs:3000',
  exitPassword : 'aditabs_exit',
  appVersion: '1.0.5' // minor screensaver updates
}

var heartBeat = {
  shortBeat : 1000, // 1 second
  midBeat : 60000, // 1 minute
  longBeat : 3600000 // 1 hour
}

var backgroundImageCount = globalCfg[ 'backgroundImageCount' ];

////////////////////////////////////////////////////
// Device Ready
////////////////////////////////////////////////////
document.addEventListener( "deviceready", onDeviceReadiness, false );

function onDeviceReadiness() {

  if( typeof window.localStorage.uuid === 'undefined'){
    // console.log( 'UUID IS UNDEFINED :: ' + window.device.uuid );

    var uuid = window.device.uuid;
    var data = { uuid: uuid };

    $.ajax({
      type: "POST",
      url: "http://quicktoast.co.za/pods/check_store.php", data: data,
      success: function(data){

        if( data !== 'unset' ){

          data = JSON.parse( data );
          // console.log( 'TESTING DATA VAR::: ' + data );
          window.localStorage.store = data.storeid;
          window.localStorage.screensaverStoreType = data.storetype;

          window.localStorage.uuid = uuid;

        } else {
          // console.log( 'PLEASE SET STORE TYPE MANUALLY' );
        }

      },

      error: function(XMLHttpRequest, textStatus, errorThrown) {
        // console.log('Something worse than terrible happened');
      }
    });

  }

  $( '.step6-next-button-cancel' ).click( function() {
    logActionLocal(23);  //Join: Step 6 - Clicked skip
    // event.preventDefault();
  });

  console.log("Device ready ...");
  $( '.right' ).click( function() {
    logActionLocal(5);  //Join: Step 1
    // event.preventDefault();
  });

  $( '.left' ).click( function() {
    logActionLocal(11); //Add card: Step 1
    // event.preventDefault();
  });

  $( '#home-button' ).click( function() {
    logActionLocal(13); //Clicked home
    // event.preventDefault();
  });

  $( '#manual' ).click( function() {
    logActionLocal(21); //Clicked home
    // event.preventDefault();
  });

  setTimeout( launchTouch, 1000 );

  window.plugins.insomnia.keepAwake();
}

var db;

////////////////////////////////////////////////////
// Document Ready
////////////////////////////////////////////////////

$(document).ready(function() {











  //SCREENSAVER GENERATOR START

  var screensaverGenerator = $( '#screensaver-generator' );

    // Show all slides in the screensaver
  screensaverGenerator.append( '<a href="index.html">'
    +'<div id="screensaver-all" class="screensaver all-scrsvr">'
      +'<div><img src="img/screensavers/screen_4.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_10.jpg" /></div>'
    +'</div>'
  +'</a>');

  // Shows factory screens
  screensaverGenerator.append( '<a href="index.html">'
    +'<div id="screensaver-all" class="screensaver factory-scrsvr">'
      +'<div><img src="img/screensavers/screen_1.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_2.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_3.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_4.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_5.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_6.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_7.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_8.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_9.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_10.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_11.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_12.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_13.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_14.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_15.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_16.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_17.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_18.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_19.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_20.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_21.jpg" /></div>'
   +'</div>'
  +'</a>' );

  // Shows originals screens. -->
  screensaverGenerator.append( '<a href="index.html">'
    +'<div id="screensaver-all" class="screensaver originals-scrsvr">'
      +'<div><img src="img/screensavers/screen_1.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_2.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_4.jpg" /></div>'
    +'</div>'
  +'</a>' );

  // Shows performance screens. -->
  screensaverGenerator.append( '<a href="index.html">'
    +'<div id="screensaver-all" class="screensaver performance-scrsvr">'
      +'<div><img src="img/screensavers/screen_1.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_2.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_10.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_11.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_12.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_14.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_17.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_18.jpg" /></div>'
      +'<div><img src="img/screensavers/screen_21.jpg" /></div>'
    +'</div>'
  +'</a>' );

  //SCREENSAVER GENERATOR END













  // 1 second heartbeat
  var shortHeartbeat = setInterval( function(){

    console.log( 'shortHeartbeat' );
    checkConnection();

  }, heartBeat[ 'shortBeat' ] );

  // 1 minute heartbeat
  var midHeartbeat = setInterval( function(){

    console.log( 'midHeartbeat' );
    dailyUpdates();

  }, heartBeat[ 'midBeat' ] );

  // 1 hour heartbeat
  var longHeartbeat = setInterval( function(){

    console.log( 'longHeartbeat' );
    dailyUpdates();

  }, heartBeat[ 'longBeat' ] );



















  $( '#back-btn-step2' ).click(function(){
    $( '#tab-step1-next-checking-hidden' ).trigger( 'click' );
  });

  $( '#back-btn-step3' ).click(function(){
    $( '#tab-step2-next-checking-hidden' ).trigger( 'click' );
  });

  $( '#back-btn-step4' ).click(function(){
    $( '#tab-step3-next-checking-hidden' ).trigger( 'click' );
  });

  $( '#back-btn-step5' ).click(function(){
    $( '#tab-step4-next-checking-hidden' ).trigger( 'click' );
  });


  $( '#screensaver_warning' ).click( function(){
    $( '#screensaver_warning' ).modal( 'hide' );
  });

  $( '#terms_and_conditions' ).click( function(){
    $( '#terms_and_conditions' ).modal( 'hide' );
  });

  $( '#terms_and_conditions_link' ).click( function(){
    $( '#terms_and_conditions' ).modal( 'show' );
  });

  setTimeout( launchTouch, 1000 );

	////////////////////////////////////////////////////
	// Home page administrator access
	////////////////////////////////////////////////////

	// FRONT PAGE SETTINGS BUTTON
	$( '#front-page-settings-toggle' ).click( function(){
		$( '#front-page-settings' ).stop().fadeToggle( 'fast' );
		$( '#front-page-settings-input' ).val("").focus();
	});

	$( '#front-page-settings-input' ).focus(function(){

		$( '#landing-page' ).addClass( 'longbody' );
		$('html,body').animate({scrollTop: $('html,body').height()}, 1000);

	});

	$( '#front-page-settings-input' ).blur(function(){

		$( '#landing-page' ).removeClass( 'longbody' );
		$( '#front-page-settings' ).hide( 'fast' );

	});

	$("#front-page-settings-input").keypress(function() {

		if( $("#front-page-settings-input").val() == globalCfg[ 'exitPassword' ] ) {
      $( '#front-page-settings-toggle' ).trigger( 'click' );
			$('#modalLink').trigger('click');
		}

    if( $("#front-page-settings-input").val() == globalCfg[ 'adminPassword'] ) {
      getStores();

      $( '#front-page-settings-toggle' ).trigger( 'click' );
      $('#modalLinkadmin').trigger('click');
    }

	});

	$("#close_settings").click(function() {
		//var db = window.openDatabase("a3s", "1.0", "a3s", 200000);
    $( '.admin_stores_screen' ).hide();
    $( '.admin_loading_screen' ).show();
		setStore();
    logActionLocal(14);

	});

	$("#exit_app, #exit_app_admin").click(function() {
    logActionLocal(15);
    navigator.app.exitApp();
  });

	//////////////////////////////////////////////////////
  // STEP 1 START.                                    //
  //////////////////////////////////////////////////////

  $( '#step1-next-button' ).click( function(){

    var emailError = "Please enter your Email Address";
    var mobileError = "Please enter your Mobile Number";
    var step1Counter = 0;

    if ( $( '#email' ).val() == "" ){
      $( '.email' ).html( emailError );
    } else {
      if( checkMail( $( '#email' ).val() ) ){
        $( '.email' ).html( '&nbsp;' );
        step1Counter++;
      } else {
        $( '.email' ).html( 'Please enter a valid Email Address' );
      }
    }

    if ( $( '#mobile' ).val() == "" ){
      $( '.mobile' ).html( mobileError );
    } else {
      if( checkMobile( $( '#mobile' ).val() ) ){
        $( '.mobile' ).html( '&nbsp;' );
        step1Counter++;
      } else {
        $( '.mobile' ).html( 'Please enter a valid Mobile Number (eg. 081 234 5678)' );
      }
    }

    if ( step1Counter == 2 ){
      $( '#tab-step1-checking-hidden' ).trigger( 'click' );
      checkEmailAndMobile();
    }

    console.log('Step1 next button was clicked');
  });

  //////////////////////////////////////////////////////
  // STEP 1 ADD CARD START.                           //
  //////////////////////////////////////////////////////

  $( '#step1-next-button-add-card' ).click( function(){

    var emailError = "Please enter your Email Address or Mobile Number";
    var step1AddCardCounter = 0;
    var cardEmailMobile = $( '#emailmobile' ).val();

    if ( cardEmailMobile == "" ){
      $( '.emailmobile' ).html( emailError );
    } else {

      if( checkMail( cardEmailMobile ) || checkMobile( cardEmailMobile ) ){
        step1AddCardCounter++;
      } else {
        $( '.emailmobile' ).html( emailError );
      }
    }

    if ( step1AddCardCounter == 1 ){
      checkEmailOrMobile();
    }
  });

  //////////////////////////////////////////////////////
  // STEP 1 ADD CARD MANUAL START.                           //
  //////////////////////////////////////////////////////

  $( '#step1-next-button-add-card-manual' ).click( function(){

    var emailError = "Please enter your Card Number";
    var step1AddCardCounter = 0;
    var cardEmailMobile = $( '#card_manual' ).val();

    if ( cardEmailMobile == "" ){
      $( '.emailmobile' ).html( emailError );
    }

    submitCardManual();
  });

  //////////////////////////////////////////////////////
  // STEP 2 ADD CARD to step 2                        //
  //////////////////////////////////////////////////////

  // $( '#try_again_2, #try_again_1' ).click( function(){
  //   // $( '#tab-step2-next-checking-hidden' ).trigger( 'click' );
  // });

  //////////////////////////////////////////////////////
  // STEP 2 START.                                    //
  //////////////////////////////////////////////////////

  $( '#step2-next-button' ).click( function(){

    var nameError = "Please enter your Name";
    var surnameError = "Please enter your Surname";
    var step2Counter = 0;

    if ( $( '#name' ).val() == "" ){
      $( '.name' ).html( nameError );
    } else {
        $( '.name' ).html( '&nbsp;' );
        step2Counter++;
    }

    if ( $( '#surname' ).val() == "" ){
      $( '.surname' ).html( surnameError );
    } else {
        $( '.surname' ).html( '&nbsp;' );
        step2Counter++;
    }

    if ( step2Counter == 2 ){
      $( '#tab-step3-next-checking-hidden' ).trigger( 'click' );
      console.log('Step2 next button was clicked');
      logActionLocal(7);  //Join: Step 3
    }

  });

  //////////////////////////////////////////////////////
  // STEP 3 START.                                    //
  //////////////////////////////////////////////////////

  $( '#step3-next-button' ).click( function(){

    var genderError = "Please select your Gender";
    var dobError = "Please select your Date of Birth";
    var regionError = "Please select your Province";
    var step3Counter = 0;

    if( $( '#gender_female' ).is(':checked') || $( '#gender_male' ).is(':checked') ){
      $( '.gender' ).html( '&nbsp;' );
      step3Counter++;
    } else {
      $( '.gender' ).html( genderError );
    }


    if( $( '#region' ).val() != "" ){
      $( '.region' ).html( '&nbsp;' );
      step3Counter++;
    } else {
      $( '.region' ).html( regionError );
    }

    if( $( '#dob' ).val() != "" ){
      $( '.dob' ).html( '&nbsp;' );
      step3Counter++;
    } else {
      $( '.dob' ).html( dobError );
    }

    if ( step3Counter == 3 ){
      $( '#tab-step4-next-checking-hidden' ).trigger( 'click' );
      var x = setTimeout('$("#password").focus()', 700);
      console.log('Step3 next button was clicked');
      logActionLocal(8);  //Join: Step 4
    }

  });

  //////////////////////////////////////////////////////
  // STEP 4 START.                                    //
  //////////////////////////////////////////////////////

  $( '#step4-next-button' ).click( function(){

    var passwordError = "Please enter your Password";
    var passwordConfirmError = "Please confirm your Password";
    var step4Counter = 0;

    if ( $( '#password' ).val() == "" ){
      $( '.password' ).html( passwordError );
    } else {
      if( checkPassword( $( '#password' ).val() ) ){
        $( '.password' ).html( '&nbsp;' );
        step4Counter++;
      } else {
        $( '.password' ).html( 'Please enter a valid Password (Minimum of 4 characters)' );
      }
    }

    if( $( '#password-confirm' ).val() == $( '#password' ).val() ){
      step4Counter++;
    } else {
      $( '.password-confirm' ).html( 'These Passwords do not match' );
    }

    if ( step4Counter == 2 ){
      $( '#tab-step5-next-checking-hidden' ).trigger( 'click' );
      console.log('Step4 next button was clicked');
      logActionLocal(9);  //Join: Step 5
    }

  });

  //////////////////////////////////////////////////////
  // STEP 5 START.                                    //
  //////////////////////////////////////////////////////

  $( '#step5-next-button' ).click(function(){

    if( $( '#terms' ).is( ":checked" ) ){
      submitRegistration();
      $( '#tab-step6-next-checking-hidden' ).trigger( 'click' );

    } else {
      alert('Please accept our Terms and Conditions');
    }

  });

  // click to select checkbox
  $( '.performance' ).click( function(){
    if( $( '.performance input[type="checkbox"]' ).is(":checked") ){
      $( '.performance input[type="checkbox"]' ).prop('checked', false);
    } else {
      $( '.performance input[type="checkbox"]' ).prop('checked', true);
    }
  });

  // checkbox bug fix.
  $( '.performance input[type="checkbox"]' ).click( function(){
    if( $( '.performance input[type="checkbox"]' ).is(":checked") ){
      $( '.performance input[type="checkbox"]' ).prop('checked', false);
    } else {
      $( '.performance input[type="checkbox"]' ).prop('checked', true);
    }
  });

  $( '.originals' ).click( function(){
    if( $( '.originals input[type="checkbox"]' ).is(":checked") ){
      $( '.originals input[type="checkbox"]' ).prop('checked', false);
    } else {
      $( '.originals input[type="checkbox"]' ).prop('checked', true);
    }
  });

  // checkbox bug fix.
  $( '.originals input[type="checkbox"]' ).click( function(){

    console.log('clicked');

    if( $( '.originals input[type="checkbox"]' ).is(":checked") ){
      $( '.originals input[type="checkbox"]' ).prop('checked', false);
    } else {
      $( '.originals input[type="checkbox"]' ).prop('checked', true);
    }

  });

  $( '#sport7' ).click( function(){
    if ( $( '#sport7' ).is(":checked") ){
      $( '#sport_other' ).removeAttr( 'disabled' );
      $( '#sport_other' ).focus();
    } else {
      $( '#sport_other' ).attr('disabled', true);
      $( '#sport_other' ).val( '' );
    }
  });

  $( '#sport_other' ).click(function(){
    $( '#sport7' ).prop('checked', true);
  });

	////////////////////////////////////////////////////
	// Submission listener
	////////////////////////////////////////////////////

	////////////////////////////////////////////////////
	// Close soft keyboard when scanning for a barcode
	////////////////////////////////////////////////////

	$("#card").focus(function() {
		console.log("Focusing ...");
		cordova.plugins.Keyboard.close();	//cordova plugin add https://github.com/driftyco/ionic-plugins-keyboard
	});

  // check connection on the device to change the connection
  // icon the relevant icon
  var ConnectionCheck = setInterval( function(){
    // checkConnection();
  }, globalCfg['checkConnectionDelay'] );

  // Screensaver Timer
  var timeElapsed;
  var countdown = 5;
  var checkTime = setInterval( function(){

    var screenTimer = new Date();
    var screenTimerhours = screenTimer.getHours() * 3600;
    var screenTimerminutes = screenTimer.getMinutes() * 60;
    var screenTimerseconds = screenTimer.getSeconds();
    var screenTimertimeElapsed = screenTimerhours + screenTimerminutes + screenTimerseconds;

    // console.log( screenTimertimeElapsed );
    // console.log( location.pathname );

    if( screenTimertimeElapsed == timeElapsed + globalCfg['screensaverWarning'] ){
        $( '#terms_and_conditions' ).modal( 'hide' );
        $('#screensaver_warning').modal('show');
        // console.log( 'Screensaver warning started.' );
        scrsvrCountdown();
    }

    if( screenTimertimeElapsed == timeElapsed + globalCfg['screensaverDelay'] ){
      // cordova.plugins.Keyboard.close();
      console.log( 'Screensaver started.' );

      if( typeof window.localStorage.screensaverStoreType == 'undefined' ){
        $( '.all-scrsvr' ).fadeIn();
      } else {
        switch( window.localStorage.screensaverStoreType ){
          case 'Originals':
          case 'originals':
          $( '.originals-scrsvr' ).fadeIn();
          break;
          case 'Performance':
          case 'performance':
          $( '.performance-scrsvr' ).fadeIn();
          break;
          case 'Both':
          case 'both':
          case 'all':
          case 'All':
          $( '.all-scrsvr' ).fadeIn();
          break;
          case 'Factory':
          case 'factory':
          $( '.factory-scrsvr' ).fadeIn();
          break;
          case 'Concept':
          case 'concept':
          $( '.factory-scrsvr' ).fadeIn();
          break;
          case 'Kids':
          case 'kids':
          $( '.kids-scrsvr' ).fadeIn();
          break;
          case 'Development':
          case 'development':
          case 'dev':
          case 'Dev':
          $( '.all-scrsvr' ).fadeIn();
          break;
        }
      }

      logActionLocal(1); //Screensaver started
      logActionLocal(24); //Screensaver started

      setInterval(dailyChecks, 30000);
      setInterval(dailyUpdates, 50000);
    }

  }, 1000);

  ////////////////////////////////////////////////////
  // Screensaver initiate
  ////////////////////////////////////////////////////

  if( typeof window.localStorage.screensaverStoreType !== 'undefined' ){

    console.log( 'local storage screensaverStoreType: ' + window.localStorage.screensaverStoreType );

    switch( window.localStorage.screensaverStoreType ){

      case 'Originals':
      case 'originals':
      console.log( 'originals' );
      var ss1 = setInterval(function() {
        $('.originals-scrsvr  > div:first')
          .animate( { opacity: 0 }, 1000 )
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('.originals-scrsvr');

          // console.log( 'originals-scrsvr screensaver rotating.' );

      }, globalCfg['screensaverInterval'] );

      break;

      case 'Performance':
      case 'performance':
      console.log( 'performance' );
      var ss1 = setInterval(function() {
        $('.performance-scrsvr > div:first')
          .animate( { opacity: 0 }, 1000 )
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('.performance-scrsvr');

          // console.log( 'performance-scrsvr screensaver rotating.' );

      }, globalCfg['screensaverInterval'] );
      break;

      case 'Both':
      case 'both':
      case 'all':
      case 'All':
      console.log( 'all' );
      var ss1 = setInterval(function() {
        $('.all-scrsvr > div:first')
          .animate( { opacity: 0 }, 1000 )
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('.all-scrsvr');

          // console.log( 'all-scrsvr screensaver rotating.' );

      }, globalCfg['screensaverInterval'] );
      break;

      case 'Factory':
      case 'factory':
      console.log( 'all' );
      var ss1 = setInterval(function() {
        $('.factory-scrsvr > div:first')
          .animate( { opacity: 0 }, 1000 )
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('.factory-scrsvr');

          // console.log( 'factory-scrsvr screensaver rotating.' );

      }, globalCfg['screensaverInterval'] );
      break;

      case 'Kids':
      case 'kids':
      console.log( 'all' );
      var ss1 = setInterval(function() {
        $('.kids-scrsvr > div:first')
          .animate( { opacity: 0 }, 1000 )
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('.kids-scrsvr');

          console.log( 'kids-scrsvr screensaver rotating.' );

      }, globalCfg['screensaverInterval'] );
      break;

      case 'Development':
      case 'development':
      case 'dev':
      case 'Dev':
      console.log( 'all' );
      var ss1 = setInterval(function() {
        $('.all-scrsvr > div:first')
          .animate( { opacity: 0 }, 1000 )
          .next()
          .fadeIn(1000)
          .end()
          .appendTo('.all-scrsvr');

          console.log( 'all-scrsvr screensaver rotating.' );

      }, globalCfg['screensaverInterval'] );
      break;

    }

  }

  if ( typeof window.localStorage.screensaverStoreType == 'undefined' ){

    // console.log( 'window.localStorage.screensaverStoreType is undefined' );

    var ss1 = setInterval(function() {
      $('.all-scrsvr > div:first')
        .animate( { opacity: 0 }, 1000 )
        .next()
        .fadeIn(1000)
        .end()
        .appendTo('.all-scrsvr');

        // console.log( 'all-scrsvr/default screensaver rotating.' );

    }, globalCfg['screensaverInterval'] );

  }


  // Body click listener::
  $( 'body' ).bind( 'click', function(event) {

    var screensaverTimer = new Date();
    var hours = screensaverTimer.getHours() * 3600;
    var minutes = screensaverTimer.getMinutes() * 60;
    var seconds = screensaverTimer.getSeconds();

    timeElapsed = hours + minutes + seconds;

    // console.log( timeElapsed );
  });

  // start the random background image function
  rotateLandingBackground( backgroundImageCount );

});

////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////

////////////////////////////////////////////////////
// Screensaver
////////////////////////////////////////////////////

var lat;
var lon;

var onSuccess = function(position) {
	// alert('Latitude: '          + position.coords.latitude          + '\n' +
		  // 'Longitude: '         + position.coords.longitude         + '\n' +
		  // 'Altitude: '          + position.coords.altitude          + '\n' +
		  // 'Accuracy: '          + position.coords.accuracy          + '\n' +
		  // 'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
		  // 'Heading: '           + position.coords.heading           + '\n' +
		  // 'Speed: '             + position.coords.speed             + '\n' +
		  // 'Timestamp: '         + position.timestamp                + '\n');
	lat = position.coords.latitude;
	lon = position.coords.longitude;
	// console.log("lat1: " + lat);
	// console.log("lon1: " + lon);
};

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

////////////////////////////////////////////////////
// Card submission
////////////////////////////////////////////////////

function submitRegistration() {

	// onSuccess Callback
	// This method accepts a Position object, which contains the
	// current GPS coordinates
	//

	var e = encodeURIComponent($("#email").val());
	var cell = encodeURIComponent($("#mobile").val());
	var n = encodeURIComponent($("#name").val());
	var s = encodeURIComponent($("#surname").val());
	var dob = encodeURIComponent($("#dob").val());
	var gender = encodeURIComponent($("input:radio[name=gender]").filter(":checked").val());
	var region = encodeURIComponent($("#region").val());
	var p1 = encodeURIComponent($("#password").val());
	var p2 = encodeURIComponent($("#password-confirm").val());
	var sport = $("input[name='sport']:checked").serialize();
	var sport_other = encodeURIComponent($("#sport_other").val());
	var communication = $("input[name='communication']:checked").serialize();
	var storeid = window.localStorage.store;
  var uuid = window.device.uuid;
	// console.log("lat2: " + lat);
	// console.log("lon2: " + lon);
	var data = { e: e, cell: cell, n: n, surname: s, dob: dob, gender: gender, region: region, p1: p1, p2: p2, sport: sport, sport_other: sport_other, communication: communication, store: storeid, lat: lat, lon: lon, uuid:uuid };
	// console.log(data);

	$.ajax({
		type: "POST",
		url: "http://quicktoast.co.za/pods/register.php",
		data: data,
		success: function(data){
			window.localStorage.userexists = data;
			$("#newid").val(data);
			// console.log(data);
			$( '#step5-next-button-hidden' ).trigger( 'click' );
      logActionLocal(10);  //Join: Step 6
      logActionLocal(17);  //User registered
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			//$("#result").html(errorThrown);
		}
	});
}

function submitCardManual() {

	$( '#tab-step1-next-checking-hidden' ).trigger( 'click' );
	var c = $("#card_manual").val();
	var newid = $("#newid").val();
	var data = { newid: newid, card: c };

	// console.log(data);

	$.ajax({
		type: "POST",
		url: "http://quicktoast.co.za/pods/check_card.php",
		data: data,
		success: function(data){
			if( data == 3 ){
				window.location.href='index_not_connected.html';
			}
			if( data == 5 ){
				// console.log('5 was returned');	//Success!
				window.location.href='index_congratulations_card.html';
			} else if( data == 4 ){
				// console.log('4 was returned');	//Card already activated by other user
				$( '#tab-step2-manual-exists-hidden' ).trigger( 'click' );
			} else if( data == 1 ){
				// console.log('1 was returned');	//Card already activated by this user
				$( '#tab-step2-manual-exists-hidden' ).trigger( 'click' );
			} else if( data == 2 ){
				// console.log('2 was returned');	//Invalid number
				$( '#tab-stepx2-exists-hidden' ).trigger( 'click' );
			} else {
				$( '#tab-step-manual-exists-hidden' ).trigger( 'click' );	//Error, probably no network
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$("#result").html(errorThrown);
		}
	});
}

////////////////////////////////////////////////////
// Input Validation
////////////////////////////////////////////////////

function checkPassword( passwordValue ){

  // console.log( passwordValue );
  if ( passwordValue.length < 4 ){
      return false;
    } else {
      return true;
    }
}

function checkMail( emailValue ){

  var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;

  if ( re.test( emailValue ) ){
    return true;
  } else {
    return false;
  }
}

function checkMobile( mobileValue ){

  mobileValue = mobileValue.trim();
  mobileValue = mobileValue.replace( / /g,"" );

  if( isNaN( mobileValue ) ){
    return false;
  } else {
    if ( mobileValue.length !== 10 ){
      return false;
    } else {
      return true;
    }
  }
}

function checkEmailAndMobile() {

	var e = $( "#email" ).val();
	var c = $( "#mobile" ).val();
	var data = {  email: e, cell: c };

	$.ajax({
		type: "POST",
		url: "http://quicktoast.co.za/pods/check_email_or_mobile_registration.php", data: data,

		// EXISTS
		success: function(data){

			if( data == 3 ){
				window.location.href='index_not_connected.html';
			}

			if( data == 1 ){ // if not exists
				// console.log('1 was returned');
				$( '#tab-step2-next-checking-hidden' ).trigger( 'click' );
				var x = setTimeout('$("#name").focus()', 700);	//See http://stackoverflow.com/questions/12984828/twitter-bootstrap-dropdown-input-field-not-gaining-working-with-focus-method?rq=1
        logActionLocal(6);  //Join: Step 2
			} else {
				window.localStorage.userexists = data;
				$( '#tab-step1-exists-hidden' ).trigger( 'click' );
        logActionLocal(16);  //Join: Step 1 - User exists
			}

		},

		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$(".email").html(errorThrown);
		}

	});
}

function checkEmailOrMobile() {

	$( '#step1-next-button-add-card-hidden' ).trigger( 'click' );
	var e = $("#emailmobile").val();
	var data = {  email: e };

	$.ajax({
		type: "POST",
		url: "http://quicktoast.co.za/pods/check_email_or_mobile_registration2.php", data: data,

		// EXISTS
		success: function(data){
			// $(".email").html("positive");

			if( data == 3 ){
				// console.log('3 was returned');
				window.location.href='index_not_connected.html';
			}
			else if( data == 2 ){
				// console.log('2 was returned');
				$( '#tab-step1-not-exists-hidden' ).trigger( 'click' );
        logActionLocal(22);  //Add card: Step 1 - User doesn't exist
			}
			else {
				// console.log('id: ' + data + ' was returned');
				window.localStorage.userexists = data;
				$("#newid").val(data);
				$( '#tab-step2-next-checking-hidden' ).trigger( 'click' );
			}
		},

		error: function(XMLHttpRequest, textStatus, errorThrown) {
			$(".email").html(errorThrown);
		}
	});
}

////////////////////////////////////////////////////
// Set store
////////////////////////////////////////////////////

function setStore() {

	var storeid = $("#storeid").val();
	// console.log("storeid: " + storeid);

	window.localStorage.store = storeid;
	// console.log("store: " + window.localStorage.store);
  var uuid = window.device.uuid;
  var data = { storeid: storeid, uuid: uuid };

  $.ajax({
    type: "POST",
    url: "http://quicktoast.co.za/pods/screensaver_type.php",
    data: data,
    success: function( data ){
      var data = JSON.parse(data);
      window.localStorage.screensaverRegion = data.screensaverRegion;
      window.localStorage.screensaverStoreType = data.screensaverStoreType;
      // console.log( window.localStorage.screensaverRegion );
      // console.log( window.localStorage.screensaverStoreType );
      window.location.href = "/android_asset/www/index.html";
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
    }
  });
}

////////////////////////////////////////////////////
// Connection
////////////////////////////////////////////////////

function checkConnection() {

	// console.log( 'Checked connection' );

    var networkState = navigator.connection.type;
    var states = {};

    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

  switch ( states[networkState] ){

    case states[Connection.UNKNOWN] :
      // console.log( 'Current tested connection state: ' + states[Connection.UNKNOWN] );
      toggleWiFiIcon( 'disconnected' );

      if( !urlRedirect() ){
        window.location.href = "/android_asset/www/index_not_connected.html";
      }
    break;

    case states[Connection.ETHERNET] :
      // console.log( 'Current tested connection state: ' + states[Connection.ETHERNET] );
      toggleWiFiIcon( 'connected' );

      if( urlRedirect() ){
        window.location.href = "/android_asset/www/index.html";
      }
    break;

    case states[Connection.WIFI] :
      // console.log( 'Current tested connection state: ' + states[Connection.WIFI] );
      toggleWiFiIcon( 'connected' );

      if( urlRedirect() ){
        window.location.href = "/android_asset/www/index.html";
      }

    break;

    case states[Connection.CELL_2G]:
    case states[Connection.CELL_3G]:
    case states[Connection.CELL]:
    case states[Connection.CELL_4G]:
      // console.log( 'Current tested connection state: ' + states[Connection.CELL_2G] );
      toggleWiFiIcon( 'connected' );

      if( urlRedirect() ){
        window.location.href = "/android_asset/www/index.html";
      }
    break;

    case states[Connection.NONE] :
      // console.log( 'Current tested connection state: ' + states[Connection.NONE] );
      toggleWiFiIcon( 'disconnected' );

      if( !urlRedirect() ){
        window.location.href = "/android_asset/www/index_not_connected.html";
      }

    break;
  }
}

function toggleWiFiIcon( conStatus ){

  switch( conStatus ){

    case 'disconnected' :
    if( $( '.wifi-icon' ).hasClass( 'green-wifi' ) ){
      $( '.wifi-icon' ).removeClass( 'green-wifi' );
      $( '.wifi-icon' ).addClass( 'red-wifi' );
    } else {
      $( '.wifi-icon' ).addClass( 'red-wifi' );
    }
    break;

    case 'connected' :
    if( $( '.wifi-icon' ).hasClass( 'red-wifi' ) ){
      $( '.wifi-icon' ).removeClass( 'red-wifi' );
      $( '.wifi-icon' ).addClass( 'green-wifi' );
    } else {
      $( '.wifi-icon' ).addClass( 'green-wifi' );
    }
    break;
  }

}

function urlRedirect(){

  if( location.pathname == "/android_asset/www/index_not_connected.html" ){
    return true
  } else {
    return false;
  }

}

function rotateLandingBackground( backgroundImageCount ){
  var backgroundCounter = Math.floor( ( Math.random() * backgroundImageCount ) + 1 );
  var backgroundImageCSS = 'img/backgrounds/' + backgroundCounter + '.jpg';

  $( '#landing-page' ).css( {'background-image' : 'url(' + backgroundImageCSS + ')'} );
}

//body was touched to initiate the screensaver countdown from load.
function launchTouch(){
  $( 'body' ).trigger( 'click' );
  // console.log( 'body was touched.' );
  if( location.pathname == "/android_asset/www/index.html" ){
    logActionLocal(2);  //Screensaver ended
  }
}

//Countdown timer for the screensaver.
function scrsvrCountdown( screensaverWarning ){
  var screensaverWarning = 10;
  $( '#countdown-timer' ).html( screensaverWarning );
  var countdownInterval = setInterval( function(){
    $( '#countdown-timer' ).html( screensaverWarning );
    screensaverWarning = screensaverWarning - 1;
    if( screensaverWarning == 0 ){
      clearInterval( countdownInterval );
      $( '#countdown-timer' ).html( '' );
    }
  }, 1000);
}

//Log actions
function logAction(actionid, datelogged) {
  var a = actionid;
  var s = window.localStorage.store;
  var d = datelogged;
  //logActionLocal(a);
  var data = { s: s, a: a, d: d, v: globalCfg[ 'appVersion' ] };

  $.ajax({
    type: "POST",
    url: "http://quicktoast.co.za/pods/log_pods.php", data: data,
    success: function(data){
      if( data == "" ){
        // console.log('Something terrible happened');
      }
      else {
        // console.log(data + ' was returned');
      }
    },

    error: function(XMLHttpRequest, textStatus, errorThrown) {
      // console.log('Something worse than terrible happened');
    }
  });
}

//Log dailylog
function logActionDailyLog(dailylog) {
  var s = window.localStorage.store;
  var d = dailylog;
  //logActionLocal(a);
  var data = { s: s, a: 0, d: d, v: globalCfg[ 'appVersion' ] };

  $.ajax({
    type: "POST",
    url: "http://quicktoast.co.za/pods/log_pods.php", data: data,
    success: function(data){
      if( data == 0 ){
        // console.log('Something terrible happened');
      }
      else {
        // console.log(data + ' was returned');
        window.localStorage.dailylog = "";
      }
    },

    error: function(XMLHttpRequest, textStatus, errorThrown) {
      // console.log('Something worse than terrible happened');
    }
  });
}

//Log dailylog___ DEPRECATED FUNCTION
// function logActionDailyBrochureLog( dailylog ) {
//   var s = window.localStorage.store;
//   var d = dailylog;
//   //logActionLocal(a);
//   var data = { s: s, a: 0, d: d, v: globalCfg[ 'appVersion' ] };

//   $.ajax({
//     type: "POST",
//     url: "http://quicktoast.co.za/pods/log_pods_brochure.php", data: data,
//     success: function(data){
//       window.localStorage.logBrochure = "";
//     },

//     error: function(XMLHttpRequest, textStatus, errorThrown) {
//     }
//   });
// }

function logActionLocal(actionid) {
  // console.log("Beginning to log actionid: " + actionid);
  var a = actionid;
  var s = window.localStorage.store;
  var d = Math.round(new Date().getTime() / 1000);
  // var db = window.sqlitePlugin.openDatabase({name: "pods.db"});

  if (!window.localStorage.dailylog) {
  window.localStorage.dailylog = "";
  }
  var l = window.localStorage.dailylog;
  l += d + ":" + a + ",";
  window.localStorage.dailylog = l;
  // console.log("dailylog: " + l);
}

var dailylog1 = false;
var dailylog2 = false;

function dailyChecks() {
  // console.log('dailyChecks');
  var d = Math.round(new Date().getTime() / 1000);
  var d2 = new Date();
  var hour = d2.getHours();
  // console.log('hour: ' + hour);
  if ( !dailylog2 ) {
    dailylog2 = true;
    logAction(3, d);  //Daily checks
  }
}

function dailyUpdates() {
  // console.log('dailyUpdates');
  var d = Math.round(new Date().getTime() / 1000);
  var d2 = new Date();
  var hour = d2.getHours();
  // console.log('hour: ' + hour);
  if (!dailylog1) {
    dailylog1 = true;
    logAction(4, d);  //Daily updates

    if (!window.localStorage.dailylog) {
      window.localStorage.dailylog = "";
    }
    var l = window.localStorage.dailylog;
    logActionDailyLog(l);
    // logActionDailyBrochureLog( window.localStorage.logBrochure );
  }
}

function getStores() {

  $( '.admin_stores_screen' ).hide();
  $( '.admin_loading_screen' ).show();

  window.localStorage.userexists = 0;
  if (window.localStorage.store) {
    var storeid = window.localStorage.store;
  }
  else {
    storeid = 0;
  }
  // console.log("storeid: " + storeid);
  var data = { store: storeid };

  $.ajax({
    type: "POST",
    data: data,
    url: "http://quicktoast.co.za/pods/getstores.php",
    success: function(data){
      $("#storeid").html(data);

      $( '.admin_stores_screen' ).show();
      $( '.admin_loading_screen' ).hide();
      // console.log(data);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      // console.log(errorThrown);
    }
  });
}



function detectScreensaver( storeTypeVar ){
  if( storeTypeVar !== '' ){

    // console.log( 'storeTypeVar: ' + storeTypeVar );

    var data = { 'storeType': storeTypeVar }

    $.ajax({
      type: "POST",
      url: "http://quicktoast.co.za/pods/screensaver_type.php",
      data: data,
      success: function( data ){
        window.localStorage.screensaverRegion = data.screensaverRegion;
        window.localStorage.screensaverStoreType = data.screensaverStoreType;
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
      }
    });
  }
}
