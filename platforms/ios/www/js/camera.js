$(document).ready(function(){

	$( '#card' ).val( 0 );

	$('.scanner').click( function(){

		cordova.plugins.barcodeScanner.scan(
			function (result) {
			 $( '#card' ).val( result.text );
			}, 
			function (error) {
			  $( '#card' ).val( 0 );
			}
		);
	});
	
	var ss = setInterval( function( scanResult ){

		var cardID = $( '#card' ).val();

		// console.log( cardID );

		if( cardID == 0 ){
			// console.log( 'No card has been scanned.' );
		} else {

			if( cardID.length !== 16 ){
				// alert( 'not a valid card' );
				$( '#tab-stepx-exists-hidden' ).trigger( 'click' );
				// console.log( 'Not a valid card.' );
				$( '#card' ).val( 0 );
			} else {

				if( location.pathname == "/android_asset/www/index2.html" ){
					$( '#tab-step1-checking-hidden' ).trigger( 'click' );
				} else {
					$( '#step1-next-button-add-card-hidden' ).trigger( 'click' );
				}

				
				clearInterval( ss );				
				// console.log( 'A valid card has been submitted.' );
				submitCardJoin( cardID );
			}
		}
		// console.log( cardID );		
	}, 1000 );

});

function submitCardJoin( cardValue ) {

  // $( '#tab-step2-next-checking-hidden' ).trigger( 'click' );
  var newid = $("#newid").val();
  var data = { newid: newid, card: cardValue };

  // console.log(data);

  $.ajax({
    type: "POST",
    url: "http://quicktoast.co.za/pods/check_card.php",
    data: data,
    success: function(data){
      if( data == 3 ){
        window.location.href='index_not_connected.html';
      }
      if( data == 5 ){
        console.log('5 was returned');  //Success!
        logActionLocal(18);	//Card activated
        window.location.href='index_congratulations_card.html';
      } else if( data == 4 ){
        console.log('4 was returned');  //Card already activated by other user
        $( '#tab-stepx2-exists-hidden' ).trigger( 'click' );
        $( '#card' ).val( 0 );
      } else if( data == 1 ){
        console.log('1 was returned');  //Card already activated by this user
        $( '#tab-stepx2-exists-hidden' ).trigger( 'click' );
        $( '#card' ).val( 0 );
      } else if( data == 2 ){
        console.log('2 was returned');  //Invalid number
        // $( '#tab-stepx-exists-hidden' ).trigger( 'click' );
        $( '#card' ).val( 0 );
      } else {
        // $( '#tab-stepx-exists-hidden' ).trigger( 'click' );   //Error, probably no network
        $( '#card' ).val( 0 );
      }
      
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      // alert(errorThrown);
    }
  });
}